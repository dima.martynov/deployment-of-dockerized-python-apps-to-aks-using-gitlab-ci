# Deployment of dockerized Python apps to AKS using Gitlab CI

This project contains Ansible playbook `ansible_playbook.yml` designed to create Azure Storage Account and a Kubernetes cluster in AKS hosting Docker containers with two applications.
The first application is `fetch_and_save2blob.py` intended to get data from a free data provider and save it into Azure Storage Blob.
The second application is `present_filtered_tabular_data.py`, it downloads dataset from Azure Storage Account and filters it before rendering a simple table on HTML page using that data.

## Getting started

To get started with this project, follow these steps:

1. Clone the repository to your local machine.
2. Install the necessary dependencies, including Kubernetes and Ansible.
3. Run the Ansible playbook to create the Kubernetes cluster and apply the manifests.

## k8s_manifest folder

The `k8s_manifest` folder contains Kubernetes manifests defining the desired state of resources in the Kubernetes cluster:
* `cronjob.yaml` a Kubernetes manifest file that defines a CronJob resource useful for running periodic and recurring tasks Current CronJob creates Jobs on a time-based schedule - every hour it runs `fetch_and_save2blob.py` to get data from a free data provider and save it into Azure Storage Blob. 
* `present_app_dep.yaml` a Kubernetes manifest file that defines a Deployment, which is a resource that orchestrates the running of a specified number of instances of an application across the cluster. `spec.replicas: 2` specifies that two instances (replicas) of the application `fetch_and_save2blob.py` should be maintained on the cluster. `spec.strategy` defines the strategy used to replace old Pods by new ones. Here, it's set to `RollingUpdate`, which means it will incrementally update Pods instances with new ones. `imagePullSecrets` field is used to specify the name of the secret `tables-secret` in file `secret.yaml` containing the credentials to pull the Docker image `present_filtered_data` from a Gitlab registry.
* `present_data_svc.yaml` is a Kubernetes Service manifest. This manifest describes a set of Pods and a policy by which to access them. `spec.ports` is a list of ports that the Service should listen on. In this case, it's listening on port 5000 and forwarding traffic to `targetPort` 5000 on the Pods it selects. And it selects pods by using a Selector `spec.selector.app: present-data`: the Service identifies the Pods it should manage by looking for Pods with the label `app=present-data`. `spec.type: NodePort`: the Service will be accessible on a static port on each Node in the Kubernetes cluster. The actual port will be selected by Kubernetes from a configured range (default: 30000-32767).

## Ansible playbook ansible_playbook.yaml

The `ansible_playbook.yaml` is an Ansible playbook that automates the process of creating a Kubernetes cluster in AKS, Azure Storage account and applying the manifests from the `k8s_manifest` folder.

## Build, Test and Deploy
Use the built-in [Continuous Integration in GitLab](https://gitlab.com/assessmentgroup/testproject/-/pipelines) to test and deploy this project. It is defined in `.gitlab-ci.yml` file.

License
This project is licensed under the terms of the GNU General Public License v3.0.

## The `fetch_and_save2blob` script
The `fetch_and_save2blob` script fetches data from an API and saves the response to an Azure Blob Storage. Here's a step-by-step breakdown of how it works:

Environment Variables: The script starts by retrieving several environment variables. These include the base URL for the API (BASE_URL), pagination parameters (PAGINATION_PARAMS), a search string (SEARCH_STRING), the connection string for Azure Storage (CONNECTION_STRING_ST), the name of the Azure Blob Storage container (CONTAINER_NAME), and the name of the blob (BLOB_NAME).

Fetch Data: The fetch_and_save2blob() function is defined. This function starts by constructing the URL for the API request and making the request using the requests.get() function.

Check Response: The function checks if the response status code is 200, which indicates a successful request.

Initialize Azure Blob Storage: If the response is successful, the function initializes the Azure Blob Storage service. It gets a blob client for the specified container and blob.

Upload Data: The function tries to upload the response text (which should be the JSON data from the API) to the blob. It uses the overwrite=True option to replace the existing blob if it already exists.

Handle Exceptions: The function includes several except blocks to handle possible exceptions. If a ResourceNotFoundError or an error related to the Azure service occurs, it logs the error.

This script is designed to be run periodically to fetch the latest data from the API and save it to Azure Blob Storage. The environment variables allow you to easily change the API endpoint, the Azure Storage details, and other parameters without modifying the script itself.

## The `present_filtered_tabular_data.py` script
The `present_filtered_tabular_data` script is a Flask web application that retrieves book data from Azure Blob Storage, filters the data based on price, and presents the filtered data in a tabular format on a web page.

Here's a step-by-step breakdown of how it works:

Environment Variables: The script starts by retrieving several environment variables. These include the connection string for Azure Storage (CONNECTION_STRING_ST), the name of the Azure Blob Storage container (CONTAINER_NAME), and the name of the blob (BLOB_NAME).

Flask App: A Flask web application is created with the name web_app.

Route Definition: A route is defined for the root URL (/) of the web application. This route accepts both GET and POST requests and is associated with the present_filtered_tabular_data() function.

Fetch Data: Inside the present_filtered_tabular_data() function, the script initializes the Azure Blob Storage service. It gets a blob client for the specified container and blob, and downloads the blob content, which should be a JSON string.

Deserialize JSON: The JSON string is deserialized into a Python dictionary using json.loads(). The 'items' key of this dictionary is expected to contain a list of books.

Filter Data: The script sets a minimum book price and gets a 'filter' query string argument from the request. If this argument is true, the script filters the book data based on the price.

The code excerpt ends here, but presumably, the function goes on to render an HTML page with the filtered book data in a tabular format. This page is returned as the response to the GET or POST request.

This script is designed to be run as a web application that users can interact with to view filtered book data. The environment variables allow you to easily change the Azure Storage details and other parameters without modifying the script itself.
